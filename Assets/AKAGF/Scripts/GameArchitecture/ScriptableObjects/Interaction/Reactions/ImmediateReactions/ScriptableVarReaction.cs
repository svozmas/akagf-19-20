﻿using AKAGF.GameArchitecture.MonoBehaviours.Interaction;
using AKAGF.GameArchitecture.ScriptableObjects.Interaction.Abstracts;
using AKAGF.GameArchitecture.ScriptableObjects.Variables;

namespace AKAGF.GameArchitecture.ScriptableObjects.Interaction.Reactions.ImmediateReactions {

    public enum OPERATION {
        SET, ADD, SUBSTRACT, MULTIPLY, DIVIDE
    }

    [System.Serializable]
    public class ScriptableVarReaction : Reaction {

        public VarType varType;
        public OPERATION operation;

        public FloatVar floatVar;
        public float floatValue;

        public IntVar intVar;
        public int intValue;

        public DoubleVar doubleVar;
        public double doubleValue;

        public BoolVar boolVar;
        public bool boolValue;

        public StringVar stringVar;
        public string stringValue;

        public bool limitValue;
        public double max, min;
        public bool loopValue;


        protected override void ImmediateReaction(ref Interactable publisher) {
            switch (varType) {

                case VarType.FLOAT: setValue(floatVar, floatValue);
                    break;

                case VarType.INT: setValue(intVar, intValue);;
                    break;

                case VarType.DOUBLE: setValue(doubleVar, doubleValue);;
                    break;

                case VarType.BOOL: boolVar.value = boolValue;
                    break;

                case VarType.STRING: stringVar.value = stringValue;
                    break;

            }




        }

        private void setValue(IntVar intVar, int value) {

            int tempVar = intVar.value;

            switch (operation) {
                case OPERATION.SET: tempVar = value;
                    break;
                case OPERATION.ADD: tempVar += value;
                    break;
                case OPERATION.SUBSTRACT: tempVar -= value;
                    break;
                case OPERATION.MULTIPLY: tempVar *= value;
                    break;
                case OPERATION.DIVIDE:
                    if(value != 0) tempVar /= value;
                    break;
            }

            if (limitValue) {

                if (!loopValue) {
                    if (tempVar > (int)max)
                        tempVar = (int)max;

                    else if (tempVar < (int)min)
                        tempVar = (int)min;
                }
                else {

                    tempVar = intVar.value;

                    if (operation == OPERATION.ADD) {

                        for (int i = 0; i <= value; i++) {

                            if (tempVar == (int)max)
                                tempVar = (int)min;
                            else
                                tempVar += 1;

                        }
                    }
                    else if (operation == OPERATION.SUBSTRACT) {

                        for (int i = value; i >= 0; i--) {

                            if (tempVar == (int)min)
                                tempVar = (int)max;
                            else
                                tempVar -= 1;

                        }
                    }
                }
            }

            // Set the correct value
            intVar.value = tempVar;
        }

        private void setValue(FloatVar floatVar, float value) {

            float tempVar = floatVar.value;

            switch (operation) {
                case OPERATION.SET:
                    tempVar = value;
                    break;
                case OPERATION.ADD:
                    tempVar += value;
                    break;
                case OPERATION.SUBSTRACT:
                    tempVar -= value;
                    break;
                case OPERATION.MULTIPLY:
                    tempVar *= value;
                    break;
                case OPERATION.DIVIDE:
                    if (value != 0) tempVar /= value;
                    break;
            }

            if (limitValue) {
              if (tempVar > max)
                tempVar = (float)max;

              else if (tempVar < min)
                tempVar = (float)min;
            }

            // Set the correct value
            floatVar.value = tempVar;
        }

        private void setValue(DoubleVar doubleVar, double value) {

            double tempVar = doubleVar.value;

            switch (operation) {
                case OPERATION.SET:
                    tempVar = value;
                    break;
                case OPERATION.ADD:
                    tempVar += value;
                    break;
                case OPERATION.SUBSTRACT:
                    tempVar -= value;
                    break;
                case OPERATION.MULTIPLY:
                    tempVar *= value;
                    break;
                case OPERATION.DIVIDE:
                    if (value != 0) tempVar /= value;
                    break;
            }

            if (limitValue) {
                if (tempVar > max)
                    tempVar = max;

                else if (tempVar < min)
                    tempVar = min;
            }

            // Set the correct value
            doubleVar.value = tempVar;
        }

    }
}

