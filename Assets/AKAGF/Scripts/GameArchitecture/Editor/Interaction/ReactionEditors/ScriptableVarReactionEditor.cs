﻿using AKAGF.GameArchitecture.ScriptableObjects.Interaction.Reactions.ImmediateReactions;
using AKAGF.GameArchitecture.ScriptableObjects.Variables;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScriptableVarReaction))]
public class ScriptableVarReactionEditor : ReactionEditor {

    private ScriptableVarReaction scriptableVarReaction;

    private const string tooltipText = "TODO";


    protected override void Init() {
        scriptableVarReaction = target as ScriptableVarReaction;
    }

    protected override void DrawReaction() {

        scriptableVarReaction.varType = (VarType) EditorGUILayout.EnumPopup("Var Type", scriptableVarReaction.varType);

        switch (scriptableVarReaction.varType){
            case VarType.FLOAT:
                scriptableVarReaction.floatVar = EditorGUILayout.ObjectField("Float Var", scriptableVarReaction.floatVar, typeof(FloatVar), true) as FloatVar;
                scriptableVarReaction.floatValue = EditorGUILayout.FloatField("Float Value", scriptableVarReaction.floatValue);

                scriptableVarReaction.operation = (OPERATION) EditorGUILayout.EnumPopup("Operation", scriptableVarReaction.operation);

                scriptableVarReaction.limitValue = EditorGUILayout.Toggle("Limit Value", scriptableVarReaction.limitValue);

                if (scriptableVarReaction.limitValue) {
                    scriptableVarReaction.max = EditorGUILayout.FloatField("Max", (float)scriptableVarReaction.max);
                    scriptableVarReaction.min = EditorGUILayout.FloatField("Min", (float)scriptableVarReaction.min);
                }

                break;

            case VarType.INT:
                scriptableVarReaction.intVar = EditorGUILayout.ObjectField("Int Var", scriptableVarReaction.intVar, typeof(IntVar), true) as IntVar;
                scriptableVarReaction.intValue = EditorGUILayout.IntField("Int Value", scriptableVarReaction.intValue);

                scriptableVarReaction.operation = (OPERATION)EditorGUILayout.EnumPopup("Operation", scriptableVarReaction.operation);

                scriptableVarReaction.limitValue = EditorGUILayout.Toggle("Limit Value", scriptableVarReaction.limitValue);

                if (scriptableVarReaction.limitValue) {
                    scriptableVarReaction.max = EditorGUILayout.IntField("Max", (int)scriptableVarReaction.max);
                    scriptableVarReaction.min = EditorGUILayout.IntField("Min", (int)scriptableVarReaction.min);
                }

                if (scriptableVarReaction.limitValue && (scriptableVarReaction.operation == OPERATION.ADD || scriptableVarReaction.operation == OPERATION.SUBSTRACT)){
                    scriptableVarReaction.loopValue = EditorGUILayout.Toggle("Loop Value", scriptableVarReaction.loopValue);
                }

                break;


            case VarType.DOUBLE:
                scriptableVarReaction.doubleVar = EditorGUILayout.ObjectField("Double Var", scriptableVarReaction.doubleVar, typeof(DoubleVar), true) as DoubleVar;
                scriptableVarReaction.doubleValue = EditorGUILayout.DoubleField("Double Value", scriptableVarReaction.doubleValue);

                scriptableVarReaction.operation = (OPERATION)EditorGUILayout.EnumPopup("Operation", scriptableVarReaction.operation);

                scriptableVarReaction.limitValue = EditorGUILayout.Toggle("Limit Value", scriptableVarReaction.limitValue);

                if (scriptableVarReaction.limitValue) {
                    scriptableVarReaction.max = EditorGUILayout.DoubleField("Max", scriptableVarReaction.max);
                    scriptableVarReaction.min = EditorGUILayout.DoubleField("Min", scriptableVarReaction.min);
                }

                break;

            case VarType.BOOL:
                scriptableVarReaction.boolVar = EditorGUILayout.ObjectField("Bool Var", scriptableVarReaction.boolVar, typeof(BoolVar), true) as BoolVar;
                scriptableVarReaction.boolValue = EditorGUILayout.Toggle("Bool Value", scriptableVarReaction.boolValue);
                break;

            case VarType.STRING:
                scriptableVarReaction.stringVar = EditorGUILayout.ObjectField("String Var", scriptableVarReaction.stringVar, typeof(StringVar), true) as StringVar;
                scriptableVarReaction.stringValue = EditorGUILayout.TextField("String Value", scriptableVarReaction.stringValue);
                break;
        }

        Undo.RecordObject(target, "scriptableVarReactionChanged");
    }


    protected override GUIContent GetFoldoutLabel () {
        return new GUIContent("ScriptableVar Reaction", tooltipText); 
    }
}
